const net = require('net');

class IrcClient {
    constructor(host, port) {
        this.host = host;
        this.port = port;
        this.socket;
    }

    connect() {
        this.socket = net.connect({
            host: this.host,
            port: this.port
        });
        this.socket.pipe(process.stdout);
    }

    on(command, action) {
        this.socket.on('data', async data => {
            for (const message of data.toString().split('\r\n')) {
                const match = message.toString().match(/(:.+?(?= ))?(.+?(?= ))(.*)/);
                if (match == null) continue;
                const parsedMessage = { prefix: match[1].trim(), command: match[2].trim(), params: match[3].trim() }
                if (parsedMessage.command.toLowerCase() == command.toLowerCase()) {
                    console.log(`matched ${parsedMessage.command}`);
                    await action(parsedMessage);
                }
            }
        });
    }

    send(message) {
        this.socket.write(`${message}\r\n`);
    }
}

class TwitchBot {
    constructor(username, token, commands) {
        this.username = username;
        this.token = token;
        this.irc = new IrcClient('irc.chat.twitch.tv', 6667);
        this.commands = commands;
    }

    connect() {
        this.irc.connect();
        this.irc.send(`PASS ${this.token}`);
        this.irc.send(`NICK ${this.username}`);
        this.irc.on("ping", () => this.irc.send('PONG :tmi.twitch.tv'));
        this.irc.on('privmsg', async options => {
            //TODO if (isSelf) return;
            let match = options.params.match(/#(.+) :(.*)/);
            const channel = match[1];
            const message = match[2];
            const user = { username: options.prefix.match(/:(.+?)!/)[1] };

            for (const command of this.commands) {
                match = message.match(command.pattern);
                if (match) {
                    client.match = match;
                    if (command.cooldown !== undefined) {
                        const now = new Date().getTime();
                        if (command.triggeredAt === undefined || (command.triggedAt + command.cooldown * 1000) < now) {
                            command.triggeredAt = now;
                        } else {
                            continue;
                        }
                    }
                    try {
                        return await command.action.bind(client)(channel, user);
                    } catch (error) {
                        console.log(error);
                        this.say(channel, 'Something went wrong :(');
                    }
                }
            }
        });
    }

    join(channel) {
        this.irc.send(`JOIN #${channel}`);
    }

    say(channel, message) {
        this.irc.send(`PRIVMSG #${channel.toLowerCase()} :${message}`);
    }
}

module.exports = IrcClient;

const commandsCsv = process.argv[process.argv.indexOf('--commands') + 1];
const commands = commandsCsv.split(',').reduce((commands, commandPath) => {
    commands.push(...require(`./commands/${commandPath}`));
    return commands;
}, []);
const bot = new TwitchBot(process.env.USERNAME, process.env.PASSWORD, commands);
bot.connect();
bot.join('blueredbrackets');