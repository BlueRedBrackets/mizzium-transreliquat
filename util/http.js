import * as querystring from 'querystring';
import * as httpProtocol from 'http';
import * as httpsProtocol from 'https';

class Http {
    constructor(protocol) {
        this.protocol = protocol;
    }

    async request(options) {
        return new Promise((resolve, reject) => {
            if (options.payload !== undefined) {
                options.headers['Content-Length'] = options.payload.length;
            }
            if (options.path && options.query) {
                options.path += `?${querystring.stringify(options.query)}`;
            }

            const request = this.protocol.request(options, res => {
                let body = '';
                res.on('data', chunk => body += chunk);
                res.on('end', () => {
                    res.body = body;

                    res.ensureSuccess = function () {
                        if (Math.floor(res.statusCode / 100) !== 2) {
                            throw Error(`${res.statusCode} ${res.body}`);
                        }
                    }
                    resolve(res);
                });
            }).on('error', err => reject(err));
            if (options.payload !== undefined) {
                request.write(options.payload);
            }
            request.end();
        });
    }
}

export const http = new Http(httpProtocol);
export const https = new Http(httpsProtocol);