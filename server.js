import * as url from 'url';
import * as http from 'http';
import { https } from './util/http.js';
import * as querystring from 'querystring';

var server = http.createServer(async function (req, res) {
    const uri = url.parse(req.url, true);
    if (uri.pathname !== '/') return;
    const payload = querystring.stringify({
        grant_type: 'authorization_code',
        code: uri.query.code,
        redirect_uri: 'http://localhost:5000',
        client_id: process.env.CLIENT_ID,
        client_secret: process.env.CLIENT_SECRET,
    });
    console.log(https)
    const tokenResponse = await https.request({
        method: 'POST',
        protocol: 'https:',
        hostname: 'accounts.spotify.com',
        path: '/api/token',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
    }, payload);
    tokenResponse.on('data', data => console.log(data.toString()));
    res.end();
});

server.listen(5000);

const scopes = 'user-read-playback-state%20user-modify-playback-state%20user-read-currently-playing';
console.log(`https://accounts.spotify.com/authorize?response_type=code&client_id=${process.env.CLIENT_ID}&scope=${scopes}&redirect_uri=http://localhost:5000`);
