class CommandHandler {
    constructor(actionHandler, next) {
        this.actionHandler = actionHandler;
        this.next = next;
    }

    async handleCommand(context) {
        if (!await this.actionHandler.handleCommand(context) && this.next) {
            await this.next.handleCommand(context);
        }
    }
}

class ActionHandler {
    constructor(action) {
        this.action = action;
    }

    async handleCommand(context) {
        await this.action(context);
        return true;
    }
}

class HelpHandler {
    constructor(trigger, help, next) {
        this.pattern = new RegExp(`^\\?${trigger}`, "i");
        this.help = help;
        this.next = next;
    }

    async handleCommand(context) {
        if (context.message.match(this.pattern)) {
            await this.help(context);
            return true;
        }
        return await this?.next?.handleCommand(context);
    }
}

class CooldownHandler {
    constructor(cooldown, next) {
        this.cooldown = cooldown;
        this.handledAt = 0;
        this.next = next;
    }

    async handleCommand(context) {
        const now = new Date().getTime();
        if ((this.handledAt + (this.cooldown * 1000)) < now) {
            this.handledAt = now;
            return await this?.next?.handleCommand(context);
        }
    }
}

class RewardHandler {
    constructor(rewardId, next) {
        this.rewardId = rewardId;
        this.next = next;
    }

    async handleCommand(context) {
        if (context.rewardId === this.rewardId) {
            return await this?.next?.handleCommand(context);
        } else if (context.match) {
            await context.say(`@${context.user.username} that command requires you redeem a reward with channel points`);
            return true;
        }
    }
}

class PatternHandler {
    constructor(pattern, next) {
        this.pattern = pattern;
        this.next = next;
    }

    async handleCommand(context) {
        context.match = context.message.match(this.pattern);
        if (context.match) {
            return await this?.next?.handleCommand(context);
        }
    }
}

class TriggerHandler {
    constructor(trigger, next) {
        if (!Array.isArray(trigger)) trigger = [trigger];
        this.pattern = new RegExp(`^!(${trigger.join("|")})\s*`, "i");;
        this.next = next;
    }

    async handleCommand(context) {
        context.match = context.message.match(this.pattern);
        if (context.match && this.next) {
            return await this.next.handleCommand(context);
        }
    }
}

class OptionsHandler {
    constructor(options, help, next) {
        this.pattern = options;
        this.help = help;
        this.next = next;
    }

    async handleCommand(context) {
        context.match = context.message.substring(context.match[0].length).match(this.pattern);
        if (context.match && this.next) {
            return await this.next.handleCommand(context);
        }
        if (this.help) await this?.help(context);
        return true;
    }
}

export function makeCommandHandler(configs) {
    return configs.reduce(function (commandHandler, config) {
        let actionHandler = new ActionHandler(config.action);
        if (config.cooldown) actionHandler = new CooldownHandler(config.cooldown, actionHandler);
        if (config.rewardId) actionHandler = new RewardHandler(config.rewardId, actionHandler);
        if (config.options && config.help) actionHandler = new OptionsHandler(config.options, config.help, actionHandler);
        if (config.trigger) actionHandler = new TriggerHandler(config.trigger, actionHandler);
        else if (config.pattern) actionHandler = new PatternHandler(config.pattern, actionHandler);
        if (config.trigger && config.help) actionHandler = new HelpHandler(config.trigger, config.help, actionHandler);
        return new CommandHandler(actionHandler, commandHandler);
    }, undefined);
}
