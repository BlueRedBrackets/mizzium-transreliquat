import * as fs from 'fs';

class Slider {
    constructor(minValue, maxValue, initialValue) {
        this.minValue = minValue;
        this.maxValue = maxValue;
        this._value = this.initialValue = initialValue;
    }

    reset() {
        this.value = this.initialValue;
    }

    get value() {
        return this._value;
    }

    set value(newValue) {
        this._value = Math.min(Math.max(newValue, this.minValue), this.maxValue);
    }

    valueOf() {
        return this._value / this.maxValue;
    }

    toString() {
        return this.valueOf().toString();
    }
}

export class BitCrushPlugin {
    constructor() {
        this.slider = new Slider(0, 10, 0);
    }

    pluginText() {
        if (this.slider == 0) return "";
        const crushPercent = this.slider * 35;
        const chunkData = Buffer.from(`VC2!G\x04\x00\x00<Krush pluginEditorWidth="750" pluginEditorHeight="316" isMidiEnabled="0" isMidiProgramChangeEnabled="0" ccMidiChan="0"><INSTANCEPREFS/><FROZENPARAMS dry="0" wet="0" bypassDrySignal="0" bypassWetSignal="0" dryWetLink="0" drive="0" driveModDepth="0" crush="0" crushModDepth="0" downsamp="0" downsampModDepth="0" bypassFilter="0" filterType="0" lowpassFreq="0" lowpassRes="0" hipassFreq="0" hipassRes="0" filterFreqModDepth="0" filterResModDepth="0" lfoSyncOrFree="0" lfoFreeRate="0" lfoSyncedPeriod="0" lfoWaveform="0"/><PARAMS dry="-48" wet="0.30000071972608566284" bypassDrySignal="0" bypassWetSignal="0" dryWetLink="0" drive="0" driveModDepth="0" crush="${crushPercent}" crushModDepth="0" downsamp="${crushPercent}" downsampModDepth="0" bypassFilter="1" filterType="0" lowpassFreq="20000" lowpassRes="0" hipassFreq="20" hipassRes="0" filterFreqModDepth="0" filterResModDepth="0" lfoSyncOrFree="0" lfoFreeRate="0" lfoSyncedPeriod="32" lfoWaveform="0"/><PARAMSLastLoadedPreset preset="C:\ProgramData\Tritik\Krush\presets\Crushing\Cheap Toy.tkpreset" dirty="1"/><MIDIMAPPING/></Krush>`, "utf8").toString("base64");
        return `VSTPlugin: Library Krush.dll ChunkData ${chunkData}`;
    }

    toString() {
        return `Bit Crush: ${this.slider * 100}%`;
    }
}

export class VolumePlugin {
    constructor() {
        this.slider = new Slider(0, 100, 50);
    }

    get on() {
        return this.slider == this.slider.initialValue;
    }

    pluginText() {
        if (this.slider == this.slider.initialValue) return "";
        return `VSTPlugin: Library LoudMax64.dll Thresh 0.5 "ISP Detection" 0 Output ${this.slider} "Fader Link" 0`
    }

    toString() {
        return `Volume: ${this.slider * 100}%`;
    }
}

export class FrequencyShiftPlugin {
    constructor() {
        this.on = false;
        this.wetSlider = new Slider(0, 20, 20);
        this.shiftSlider = new Slider(-10, 10, 0);
        this.delaySlider = new Slider(0, 20, 0);
        this.lowSlider = new Slider(0, 20, 0);
        this.highSlider = new Slider(0, 20, 20);
    }

    //`VSTPlugin: Library freqshifter64.dll "Fq Min L" 0.5 "Link L+R" 1 "Vol L" 0.5 "Fq Range" 0.666 "Vol R" 0.5 "Mode L" 0 "Mix L" 1 "Mode R" 0 "Mix R" 1 "Fq Min R" 0.5 "Fq Max L" ${maxFrequency} "Fq Max R" ${maxFrequency} "LFO Wave" 0 "LFO Rate" 0`;

    pluginText() {
        if (this.on) {
            const chunkData = Buffer.from(`VC2!+\x01\x00\x00<?xml version="1.0" encoding="UTF-8"?> <MYPLUGINSETTINGS pluginVersion="1" wetDryMix="${this.wetSlider}" shift="${this.shiftSlider}" delay="${this.delaySlider.valueOf().toFixed(16)}" sync="0.0" feedback="0.0" lowCut="${this.lowSlider.valueOf().toFixed(16)}" highCut="${this.highSlider.valueOf().toFixed(16)}" stereo="1.0" uiWidth="530" uiHeight="260"/>\x00`).toString("base64");
            return `VSTPlugin: Library ValhallaFreqEcho_x64.dll ChunkData ${chunkData}`
        }
    }

    toString() {
        return `Frequency Shift: ${this.on ? "on" : "off"}; ${this.wetSlider * 100}% wet; ${this.shiftSlider >= 0 ? "+" : "-"}${this.shiftSlider * 1000}hz; ${this.delaySlider * 1000}ms delay; ${20 + (this.lowSlider * 2980)}Hz low cut; ${50 + (this.highSlider * 14950)}Hz high cut`;
    }
}

export class BassBoostPlugin {
    constructor() {
        this.on = false;
        this.boostSlider = new Slider(0, 20, 0);
        this.cutoffSlider = new Slider(0, 20, 0);
    }

    pluginText() {
        if (this.on)
            return `VSTPlugin: Library LOVEND.dll Boost ${this.boostSlider} Oversampling 0 Frequency ${this.cutoffSlider} Bypass 1 Output 0.5`;
    }

    toString() {
        return `Bass Boost: ${this.on ? "on" : "off"}; ${this.boostSlider * 100}% boost; ${60 + (this.cutoffSlider * 120)}hz cutoff`;
    }
}

export class ReverbPlugin {
    constructor() {
        this.on = false;
        this.sizeSlider = new Slider(0, 20, 0);
        this.wetSlider = new Slider(0, 20, 0);
    }

    pluginText() {
        if (this.on)
            return `VSTPlugin: Library DimensionPlus-64.dll Pan 0.5 Size ${this.sizeSlider} Gain ${this.wetSlider}`
    }

    toString() {
        return `Reverb: ${this.on ? "on" : "off"}; ${this.sizeSlider * 100}% size; ${this.wetSlider * 100}% wet`;
    }
}

export class ChorusPlugin {
    constructor() {
        this.on = false;
        this.slider = new Slider(0, 25, 0);
        this.delaySlider = new Slider(0, 20, 0);
        this.depthSlider = new Slider(0, 20, 0);
        this.rateSlider = new Slider(0, 20, 0);
        this.spreadSlider = new Slider(0, 20, 0);
        this.wetSlider = new Slider(0, 20, 0);
    }

    pluginText() {
        const chunkData = Buffer.from(`<?xml version="1.0" encoding="utf-8" ?><Preset version="4" progName="Default" MIDIInputDefault="1" MIDIOutputDefault="1"><inputParams count="9" p0="0" p1="+0" p2="100" p3="100" p4="${this.delaySlider * 30}" p5="${this.depthSlider * 100}" p6="${this.rateSlider * 20}" p7="${50 - (this.wetSlider * 50)}" p8="${50 + (this.wetSlider * 50)}" /><inputStrings /><inputCurves /><inputSurfaces /><inputCustomProperties /><model /><GUI version="1" useDefault="1" changedSincePresetLoaded="1"><skin version="2"><state version="3"><params pCount="2"><p0 name="theme.opacity" value="100" /><p1 name="theme.show_settings" value="0" /></params><curves cCount="0" /><surfaces sCount="0" /><strings sCount="0" /><collectionCounts cCount="0" /><properties pCount="0" /></state></skin></GUI></Preset>\x00`).toString('base64');
        return `VSTPlugin: Library "BC Chorus 4 VST(Stereo).dll" ChunkData "${chunkData}"`;
    }

    toString() {
        if (this.on)
            return `Chorus: ${this.on ? "on" : "off"}; ${this.delaySlider * 30}ms delay; ${this.depthSlider * 100}% depth; ${this.rateSlider * 20}Hz rate; ${this.spreadSlider * 100}% spread; ${50 + (this.wetSlider * 50)}% wet`;
    }
}

export class EchoPlugin {
    constructor() {
        this.slider = new Slider(0, 25, 0);
    }

    pluginText() {
        if (this.slider == 0) return "";
        return `VSTPlugin: Library "ATKUniversalDelay x64.dll" Delay ${this.slider} Blend 1 Feedforward 1 Feedback 0.5`
    }

    toString() {
        return `Echo: ${this.slider * 100}%`;
    }
}

export class PitchCorrectionPlugin {
    constructor() {
        this.slider = new Slider(0, 1, 0);
    }

    pluginText() {
        if (this.slider == 0) return "";
        return `VSTPlugin: Library GSnap.dll VibSpeed 0.09 Note00 0 Note11 1 CorThrsh 0.243697 Note08 0 MaxFreq 1 MinFreq 0 Gate 0 Speed 0 Note06 1 CorAmt 1 CorAtk 0 CorRel 1 Note01 1 Note02 0 Note03 0 Note04 1 Note05 0 Note07 0 Note09 1 Note10 0 MidiMode 0 BendAmt 0 VibAmt 0 Calib 0.5`
    }

    toString() {
        return `Autotune: ${this.slider == 1 ? "on" : "off"}`
    }
}

export class AudioMix {
    constructor(configPath, desktopAudioPlugins, micPlugins) {
        this.configPath = configPath;
        this.desktopAudioPlugins = desktopAudioPlugins;
        this.micPlugins = micPlugins;
    }

    plug(plugin) {
        this.plugins.push(plugin);
    }

    save() {
        const config = [
            "Device: Headphones; Speaker",
            ...this.desktopAudioPlugins.filter(p => p.on === undefined || p.on).map(plugin => plugin.pluginText()),
            "Device: Mic",
            ...this.micPlugins.filter(p => p.on).map(plugin => plugin.pluginText())
        ]
        fs.writeFileSync(this.configPath, config.join("\n"));
    }
}