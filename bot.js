import { default as twitch } from 'tmi.js';
import { makeCommandHandler } from './commandHandlerFactory.js';

class CommandContext {
    constructor(options) {
        const { client, channel, user, message } = options
        this.twitchClient = client;
        this.channel = channel;
        this.user = user;
        this.rewardId = user['custom-reward-id'];
        this.message = message;
        if (this.rewardId) console.log(`A reward with id ${this.rewardId} was redeemed by ${this.user.username}`)
    }

    async say(message) {
        await this.twitchClient.say(this.channel, message);
    }

    async ban(username, reason) {
        await this.twitchClient.ban(this.channel, username, reason);
    }

    async awaitMessage(handler) {
        const client = this.twitchClient;
        return new Promise((resolve, reject) => {
            async function handlerDecorator(channel, user, message, isSelf) {
                if (isSelf) return;
                channel = channel.replace(/^#/, "");
                try {
                    const context = new CommandContext({ client, channel, user, message });
                    const handled = await handler(context);
                    if (handled) {
                        client.removeListener("message", handlerDecorator);
                        resolve();
                    }
                } catch (error) {
                    console.error(error);
                    client.say(channel, `That didn't work...complain to streamer`);
                    reject(error);
                }
            }

            this.twitchClient.on("message", handlerDecorator);
        });
    }
}

async function getCommandConfigs() {
    return process.argv[process.argv.indexOf('--commands') + 1]
        .split(',')
        .reduce(async (commandsPromise, commandPath) => {
            const commands = await commandsPromise;
            const moreCommands = (await import(`./commands/${commandPath}.js`)).default;
            commands.push(...moreCommands);
            return commands;
        }, []);
}

async function main() {
    const channelsCsv = process.argv[process.argv.indexOf('--channels') + 1];
    const commandHandlerConfigs = await getCommandConfigs();
    const commandHandler = makeCommandHandler(commandHandlerConfigs);
    const client = twitch.client({
        identity: {
            username: process.env.USERNAME,
            password: process.env.PASSWORD
        },
        channels: channelsCsv.split(','),
        connection: { reconnect: true }
    });

    client.on('message', async (channel, user, message, isSelf) => {
        if (isSelf) return;
        channel = channel.replace(/^#/, "");
        const context = new CommandContext({ client, channel, user, message });
        try {
            commandHandler.handleCommand(context);
        } catch (error) {
            console.error(error);
            client.say(channel, `That didn't work...complain to streamer`);
        }
    });

    await client.connect();
    channelsCsv.split(",").forEach(async channel => await client.say(channel, "Hello, World!"));
}

main().then().catch(error => { throw error });
