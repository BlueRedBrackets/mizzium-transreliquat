import StormDB from "stormdb";
import * as querystring from 'querystring';
import { https as http } from './util/http.js';


export class LocalTokenRepository {
    constructor(client_id, client_secret) {
        this.db = new StormDB(new StormDB.localFileEngine("./data/spotify.db"));
        this.auth = Buffer.from(`${client_id}:${client_secret}`).toString('base64');
    }

    async getToken(id) {
        const token = this.db.get(id);
        if (new Date().getTime() >= (token.get('expiresAt').value() || 0)) {
            const payload = querystring.stringify({
                grant_type: 'refresh_token',
                refresh_token: token.get('refresh').value(),
                client_id: process.env.CLIENT_ID,
                client_secret: process.env.CLIENT_SECRET
            });
            const response = await http.request({
                hostname: 'accounts.spotify.com',
                path: '/api/token',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                },
                payload: payload
            });
            const refreshedToken = JSON.parse(response.body);
            token.set('token', refreshedToken.access_token)
            token.set('expiresAt', new Date().getTime() + (refreshedToken.expires_in - 60) * 1000)
            this.db.save();
        }
        return token.value().token;
    }
}

export class SpotifyClient {
    allowedSearchKeys = ['track', 'artist', 'album', 'year', 'genre']
    constructor(tokenRepo) {
        this.tokenRepo = tokenRepo;
    }

    async request(id, options) {
        options.hostname = 'api.spotify.com';
        options.headers = options.headers || {};
        options.headers.Authorization = `Bearer ${await this.tokenRepo.getToken(id)}`;
        const response = await http.request(options);
        response.ensureSuccess();
        return response;
    }

    async pause(id) {
        await this.request(id, {
            path: '/v1/me/player/pause',
            method: 'PUT',
        });
    }

    async play(id) {
        await this.request(id, {
            path: '/v1/me/player/play',
            method: 'PUT',
        });
    }

    async skip(id) {
        await this.request(id, {
            path: '/v1/me/player/next',
            method: 'POST',
        });
    }

    async queue(id, uri) {
        await this.request(id, {
            path: '/v1/me/player/queue',
            query: { uri },
            method: 'POST',
        });
    }

    async search(id, responseType, query) {
        let encodedQuery = '';
        if (typeof query === 'string') {
            encodedQuery = encodeURIComponent(query);
        } else {
            const x = [];
            if (query.hasOwnProperty('name')) {
                x.push(encodeURIComponent(query.name));
            }
            for (const key of this.allowedSearchKeys) {
                if (!query.hasOwnProperty(key)) continue;
                x.push(`${encodeURIComponent(key)}:%22${encodeURIComponent(query[key])}%22`);
            }
            encodedQuery = x.join('+');
        }

        const response = await this.request(id, {
            path: `/v1/search?type=${responseType}&q=${encodedQuery}&limit=3`,
            method: 'GET',
        });
        return JSON.parse(response.body);
    }

    async track(id) {
        const response = await this.request(id, {
            path: '/v1/me/player',
            method: 'GET',
        });
        return JSON.parse(response.body);
    }

    async recentlyPlayed(id) {
        const response = await this.request(id, {
            path: '/v1/me/player/recently-played',
            method: 'GET',
        });
        return JSON.parse(response.body);
    }

    async devices(id) {
        const response = await this.request(id, {
            path: '/v1/me/player/devices',
            method: 'GET',
        });
        return response;
    }

    async volume(id, volumeDelta) {
        const response = await this.request(id, {
            path: '/v1/me/player',
            method: 'GET',
        });
        let newVolume = JSON.parse(response.body).device.volume_percent + volumeDelta;
        newVolume = Math.max(Math.min(newVolume, 100), 0);
        await this.request(id, {
            path: `/v1/me/player/volume`,
            method: 'PUT',
            query: { volume_percent: newVolume }
        });
    }
}
