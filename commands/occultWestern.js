export default [{
        trigger: "jesseJones",
        cooldown: 60,
        async action(context) {
                await context.say("Jesse Jones and his horse companion, Jesse Jones, are a small-time, bounty-hunting duo with an uncomfortably strong bond.");
                await context.say("It was the end of a hot July day as the moans and squeals of birth crowded the air like dust.Although two mothers, a mare and a women, labored that evening, their screams melted together into a single, tortured voice.");
                await context.say("When it was done, two unlikely twins were born, marking the start of a brotherly companionship and a great adventure. Both Jesses grew up on the same cattle ranch together, and as soon as they were both of age they learned to ride. Before long, they moved as one, stepping and leaning in anticipation of the other. They would ride together everyday, aimlessly when there was no need to; they were inseparable.");
                await context.say("When Jesse was grown and tending most of the ranch in place of his crippled father, a group of wanted hunters known as the Blackblood Butchers came to rob the ranch. They took what little valubles they could find, and slaughtered the livestock, leaving Jesse with a wounded back leg and the Jones family with nothing.");
                await context.say("After nursing his horse back to health, Jesse hunted the outlaws down, shot them dead, and collected their bounty.Now, with their ranch gone and one bounty under their belt, the Jones Brothers began their new profession as bounty hunters.");
                await context.say("Jesse Jones, the Man: protects his horse like a sane man would protect his own body; will stay with his horse at all costs, preferably riding in the saddle; has a wounded left eye and damaged vision from his gun misfiring when he was boy; even-tempered and slow-talking; and doesn't believe in ghosts. Jesse Jones, the Horse: will try to stay with his rider, even against his command at times; has a fiery temper; loves carrots; and distrusts hunters.");
        }
}]