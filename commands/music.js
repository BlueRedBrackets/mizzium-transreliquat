import * as spotify from '../spotify.js';
const tokenRepo = new spotify.LocalTokenRepository(process.env.CLIENT_ID, process.env.CLIENT_SECRET);
const spotifyClient = new spotify.SpotifyClient(tokenRepo);

function songToString(song) {
    return `${song.name} by ${song.artists.map(artist => artist.name).join(", ")}`;
}

export default [
    {
        // !queue mastodon !by jarv
        trigger: "queue",
        options: /\s*(?<song>.+)\s+!by\s+(?<artist>.+)\s*/i,
        help(context) {
            context.say(`To queue a song, redeem the "Queue a Song" reward using channel points; in the prompt, enter "!queue <song> !by <artist>" where <song> is the name of the song, and <artist> is the name of the artist.`);
            context.say("e.g. !queue all star !by smash mouth");
        },
        rewardId: process.env.SONG_REQUEST_REWARD_ID,
        async action(context) {
            const { song, artist } = context.match.groups;
            const response = await spotifyClient.search(context.channel, 'track', {
                name: song,
                artist
            });
            // track not found
            if (response.tracks.items.length === 0) {
                return await context.say(`${song} by ${artist} not found >:(`)
            }
            // direct hit
            const results = response.tracks.items;
            const directHit = results.find(result => {
                return result.name.toLowerCase() == song.toLowerCase() &&
                    result.artists.some(resultArtist => resultArtist.name.toLowerCase() == artist.toLowerCase());
            });
            if (directHit) {
                await spotifyClient.queue(context.channel, directHit.uri);
                await context.say(`${songToString(directHit)} queued!`);
            } else { // many possible matches
                await context.say(`@${context.user.username} enter the number of the song you want to queue`);
                for (let i = 0; i < results.length; i++) {
                    await context.say(`${i + 1}. ${songToString(results[i])}`);
                }
                await context.awaitMessage(async _context => {
                    if (_context.user.username != context.user.username) return false;
                    if (!_context.message.match(/^\d$/)) return false;
                    const selected = results[_context.message - 1];
                    if (!selected) {
                        await context.say(`@${context.user.username} that wasn't an option`);
                        return false;
                    };
                    await spotifyClient.queue(context.channel, selected.uri);
                    await context.say(`${songToString(selected)} queued!`);
                    return true;
                });
            }
        }
    },
    {
        rewardId: process.env.SKIP_SONG_REWARD_ID,
        async action(context) {
            await spotifyClient.skip(context.channel);
        }
    },
    {
        // !track
        trigger: ["track", "song", "songName", "nowPlaying"],
        async action(context) {
            const track = await spotifyClient.track(context.channel);
            await context.say(`"${track.item.name}" by ${track.item.artists.map(a => a.name).join(',')}`);
        }
    }
]
