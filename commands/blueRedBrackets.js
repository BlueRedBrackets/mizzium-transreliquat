export default [
    {
        trigger: "challenge",
        async action(context) {
            await context.say("lichess: mteper; chess.com: TessituraAQ");
        },
    },
    {
        trigger: "discord",
        async action(context) {
            await context.say("https://discord.gg/4fcSnHj");
        },
    },
    {
        trigger: "hole",
        async action(context) {
            await context.say("https://code.golf/tongue-twisters");
        },
    },
    {
        trigger: "onlyFans",
        async action(context) {
            await context.say("Jebaited");
        }
    },
    {
        trigger: "beer",
        async action(context) {
            await context.say("Naked Threesome by Raised Grain 9/10");
        }
    }
]