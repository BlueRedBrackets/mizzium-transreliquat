import { VolumePlugin, BitCrushPlugin, BassBoostPlugin, ReverbPlugin, EchoPlugin, FrequencyShiftPlugin, ChorusPlugin, AudioMix, PitchCorrectionPlugin } from '../mixer.js'
const volume = new VolumePlugin();
const bitCrush = new BitCrushPlugin();
const bassBoost = new BassBoostPlugin();
const reverb = new ReverbPlugin();
const echo = new EchoPlugin();
const desktopFrequencyShift = new FrequencyShiftPlugin();
const chorus = new ChorusPlugin();
const pitchCorrection = new PitchCorrectionPlugin();
const micFrequencyShift = new FrequencyShiftPlugin();
const mixer = new AudioMix(
    process.env.AUDIO_CONFIG_PATH,
    [volume, bitCrush, bassBoost, reverb, echo, desktopFrequencyShift, chorus],
    [pitchCorrection, micFrequencyShift]
);

function makeEffectCommand(plugin, trigger, controls) {
    let controlText;
    if (controls.length === 2) {
        controlText = `${controls[0]} or ${controls[1]}`;
    } else {
        controlText = controls.slice(0, controls.length).join(', ');
        controlText += `, or ${controls[controls.length - 1]}`;
    }
    const helpText = `"!${trigger} on" turns on ${trigger}. To tweak a control, use "!${trigger} <control> <delta>" where <control> is ${controlText}, and <delta> is one or more +'s or -'s. The more +'s or -'s you add, the bigger the adjustment. To reset a control use "!${trigger} <control> ~". "!${trigger} off" turns off ${trigger}, but your changes are remembered in case you want to turn it back on later.`;
    return {
        trigger: trigger,
        options: new RegExp(`(?<control>${controls.join("|")}) (?<delta>\\++|\\-+|~)|(?<state>on|off)`, "i"),
        async help(context) {
            await context.say(plugin.toString());
            await context.say(helpText);
        },
        async action(context) {
            if (context.match.groups.state) {
                plugin.on = context.match.groups.state === "on";
            } else {
                const slider = plugin[`${context.match.groups.control}Slider`]
                const delta = context.match.groups.delta.length;
                switch (context.match.groups.delta[0]) {
                    case "+": slider.value += delta; break;
                    case "-": slider.value -= delta; break;
                    case "~": slider.reset();
                }
            }
            mixer.save();
            await context.say(plugin.toString());
        }
    }
}

function makeEffectAction(plugin) {
    return async function (context) {
        if (context.match.groups.delta == "~") {
            plugin.slider.reset();
        } else {
            const turnUp = context.match.groups.delta.startsWith("+");
            const delta = (turnUp ? 1 : -1) * context.match.groups.delta.length;
            plugin.slider.value += delta;
        }
        mixer.save();
        await context.say(plugin.toString());
    }
}

const effectOptions = /(?<delta>\++|\-+|\~\s*$)/;

export default [
    {
        // !autotune +
        trigger: "autoTune",
        options: effectOptions,
        async help(context) {
            await context.say(`say "!autotune +" to make me sound like a bobot; use "!autotune -" make me sound normal again`);
        },
        action: makeEffectAction(pitchCorrection)
    },
    {
        // !voice +
        trigger: "voice",
        options: effectOptions,
        async help(context) {
            await context.say(`say "!voice +" to make my voice higher; the more +'s, the higher I go...wahoo! I can see my house from here!; use -'s to bring me on down`);
        },
        action: makeEffectAction(micFrequencyShift)
    },
    {
        // !volume ++
        trigger: "volume",
        options: effectOptions,
        async help(context) {
            await context.say(`say "!volume +" to turn the volume up; the more +'s, the more volume; use -'s to turn the volume down`);
        },
        action: makeEffectAction(volume)
    },
    {
        // !echo ++
        trigger: "echo",
        options: effectOptions,
        async help(context) {
            await context.say(`say "!echo +" to increase the echo delay; the more +'s, the bigger delay; use -'s to decrease the delay`);
        },
        action: makeEffectAction(echo)
    },
    {
        // !crush ++
        trigger: "crush",
        options: effectOptions,
        async help(context) {
            await context.say(`say "!crush +" to crush some bits; the more +'s, the bits that are crushed; use -'s to uncrush some bits`);
        },
        action: makeEffectAction(bitCrush)
    },
    makeEffectCommand(desktopFrequencyShift, "pitch", ["shift", "delay", "low", "high", "wet"]),
    makeEffectCommand(bassBoost, "bass", ["boost", "cutoff"]),
    makeEffectCommand(chorus, "chorus", ["delay", "depth", "rate", "spread", "wet"]),
    makeEffectCommand(reverb, "reverb", ["size", "wet"]),
    {
        // !effects
        trigger: "effects",
        async help(context) {
            await context.say("Add effects to the music with !reverb, !bass, !crush, !pitch, and more");
            await context.say("What's happening now? !effects will report the current effect settings");
        },
        async action(context) {
            await context.say("---Desktop Audio Effects---")
            for (const plugin of mixer.desktopAudioPlugins) {
                await context.say(plugin.toString());
            }
            await context.say("---Mic Effects---")
            for (const plugin of mixer.micPlugins) {
                await context.say(plugin.toString());
            }
        }
    }
]