const atResponses = [
    "what?",
    "yes..?",
    "...no",
    "beep boop",
    "boo bop",
    "I sexually Identify as the black pieces. Ever since I was a boy I dreamed of defending against e4 OTB. People say to me that a person being the black pieces is impossible and I’m fucking dub but I don’t care, Checkmate. I’m having Hikaru inject me with calculation and melted plastic. From now on I want you guys to call me “The Black Pieces” and respect my right to be down a whole tempo. If you can’t accept me you, need to check your white privilege. Thank you for being so understanding.",
    [
        "What the fuck did you just fucking say about me, you little bitch? I'll have you know I graduated top of my class in the Navy Seals, and I've been involved in numerous secret raids on Al-Quaeda, and I have over 300 confirmed kills. I am trained in gorilla warfare and I'm the top sniper in the entire US armed forces. You are nothing to me but just another target.",
        "I will wipe you the fuck out with precision the likes of which has never been seen before on this Earth, mark my fucking words. You think you can get away with saying that shit to me over the Internet? Think again, fucker. As we speak I am contacting my secret network of spies across the USA and your IP is being traced right now so you better prepare for the storm, maggot. The storm that wipes out the pathetic little thing you call your life.",
        "You're fucking dead, kid. I can be anywhere, anytime, and I can kill you in over seven hundred ways, and that's just with my bare hands. Not only am I extensively trained in unarmed combat, but I have access to the entire arsenal of the United States Marine Corps and I will use it to its full extent to wipe your miserable ass off the face of the continent, you little shit.",
        "If only you could have known what unholy retribution your little \"clever\" comment was about to bring down upon you, maybe you would have held your fucking tongue. But you couldn't, you didn't, and now you're paying the price, you goddamn idiot. I will shit fury all over you and you will drown in it. You're fucking dead, kiddo."
    ],
    [
        `Where can i find a chess girlfriend? Are there special dating platforms for people like us? Finding these girls in reality seems to be really challenging, whenever I chat up a girl at a bar she either seems to be only into traps (which can sometimes be a little too weird), anime tv shows or vocaloid music. Granted my sample size isn’t that big but that’s my impression so far.`,
        `Now I’m not looking for anything special: 18 to 21, thin, loves chess (not just casually but plays herself and can appreciate the beauty that is 1.b4), c cups, red hair (I main the orangutan), especially into off-beat openings. However if only b cups are available, that will work out just fine too.`,
        `These girls seem to be kind of rare though, however with the help of the internet it should be theoretically easier to find them, but I’m not sure how, maybe I’m disregarding the most obvious solutions.`
    ]
]

const eightResponses = [
    "It is certain",
    "Without a doubt",
    "You may rely on it",
    "Yes definitely",
    "It is decidedly so",
    "As I see it, yes",
    "Most likely",
    "Yes",
    "Outlook good",
    "Signs point to yes",
    "Reply hazy try again",
    "Better not tell you now",
    "Ask again later",
    "Cannot predict now",
    "Concentrate and ask again",
    "Don’t count on it",
    "Outlook not so good",
    "My sources say no",
    "Very doubtful",
    "My reply is no"
]

const hikaruResponses = [
    "Hikaru is a straight up jerk; I get the chance to play him in tournament like twice a year and he picks joke openings. So BM",
    [
        "When I see Hikaru play the beautiful game that is chess, I get inspired. He makes me want to get better and continue pushing myself to improve and learn new things. When I watch Magnus, I get frustrated. All I see is a gimmicky bitch. It's not enjoyable watching Magnus play.",
        "Seeing him run down the clock against Hikaru was truly disheartening. That's not how anyone likes to see chess being played, and the crowd let him know. I hope Magnus handles the hate well, because he sure as hell wont be receiving any love from me."
    ],
    "Hikaru Nakamura isn't so great? Are you kidding me? When was the last time you saw a player with such an ability and movement with notation arrows? Hikaru puts the game in another level, and we will be blessed if we ever see a player with his skill and passion for the game again. Fischer breaks records. Magnus breaks records. Hikaru breaks the rules. You can keep your statistics. I prefer the magic.",
    [
        "Lol. I have a theory, and feel free to copy and paste this theory or just keep it in mind. Other Hikarus like Hikaru. And I find that to be true for the majority of his fanbase. Based off of @{username}'s past comments, this holds true. And has held true for the past 10-20 users I must have done this for.",
        "If you want a TL;DR, Hikaru tries way too hard. And if you don't what that means or can't see it, then I don't really see how I can help you. But there is a reason why he is generally disliked in the community. And I mean actual community (Tourneys).",
        "Not fucking le reddit and fb groups that have never actually been to a tourney in their life. Let's not even mention the fact that this drama queen / attention whore derailed Magnus's AMA and started some junior high school girl drama shit in 2020 after Chess's reawakening ROFL.",
        `But hey there will be Hikarus in the world, and there will be Magnuses.I do not hate you for being a Hikaru.That's just who you are. And although I'm not a "magnus"(though honestly almost no one is lol) even though I would not like to be your friend, I also do not dislike you on a personal level.But ya, if you wanted an explanation of Hikaru's character and why so many like him, it's nothing to do with his king's indian defense, and more so with his actual character.`,
        `Hikaru is amazing at chess and the king's indian.Hikaru himself however, has a very long history of bad stuff. Bad stuff on a completely different / shadier scale of a character than anything Magnus has done.Feel free to ask or make a new thread about him. It's been said many times before so I will not say it again. But the Magnus AMA from 2020 should be enough proof of it as it is.`,
        `Airing your dirty laundry out on one of the first time's Chess been on the big news for Esports. Why? Because someone you dislike won the world championship and is now getting all the attention away from you. And you're a jealous petty bitch, and so now you spend the next few hours ridiculing him on Reddit, taking advantage of the Reddit Hivemind and knowing fully well that actually a large portion of Reddit is fucking stupid too lol.`,
        `Rather than keeping it to Private Messages or (gasp) talking to Magnus in REAL life? Nah. Let's make a big spectacle of it online right after the world championship so you stay relevant.Great job Hikaru.I'm sure that worked out for you and that many people in the community didn't fucking hate your guts after that completely blatant showing of your true colors.`,
        `And if none of this is tugging at any of your heart strings for you, I'm sorry to say, but you are a HIkaru. But hey, at least now you know right? Lololol xD`
    ],
    [
        "I saw Hikaru at a grocery store in Florida yesterday. I told him how cool it was to meet him in person, but I didn’t want to be a douche and bother him and ask him for photos or anything. He said, “Oh, like you’re doing now?” I was taken aback, and all I could say was “Huh?” but he kept cutting me off and going “huh? huh? huh?” and closing his hand shut in front of my face.",
        "I walked away and continued with my shopping, and I heard him chuckle as I walked off. When I came to pay for my stuff up front I saw him trying to walk out the doors with like fifteen Milky Ways in his hands without paying.",
        "The girl at the counter was very nice about it and professional, and was like “Sir, you need to pay for those first.” At first he kept pretending to be tired and not hear her, but eventually turned back around and brought them to the counter.",
        "When she took one of the bars and started scanning it multiple times, he stopped her and told her to scan them each individually “to prevent any electrical infetterence,” and then turned around and winked at me.",
        "I don’t even think that’s a word. After she scanned each bar and put them in a bag and started to say the price, he kept interrupting her by yawning really loudly."
    ],
    "Hikaru Nakamura, Who art in chess Hallowed be Thy takes; Thy checkmate come, Thy will be done, online as it is OTB. Give us this day our daily puzzles, and refuse takeback our botez gambits, as we refuse takeback those who botez gambit against us; and lead us not into french exchange variation but deliver us from shit openings. 1.c4.",
    "To all the Magnus gloaters, I’ll tell you one thing: Twitch is, and always will be, Hikaru territory. OUR territory. The mods of this channel have formally endorsed Hikaru for GOAT. We will continue to control the chat with positive Hikaru messages. So before you start talking shit and bragging about your bitch’s win, I’ll have you know that we’re well versed in copypastas. Say RIP to your account if you try anything cute. Assholes."
]

const questionMarks = [
    "\\?",
    "¿",
    "⸮",
    "？",
    "¿",
    "⁇",
    "﹖",
    "⁈",
    "⁉",
]

function random(l) {
    let response = l[Math.floor(Math.random() * l.length)];
    if (!Array.isArray(response)) response = [response];
    return response;
}

export default [
    {
        pattern: new RegExp(`@${process.env.USERNAME}`, 'i'),
        async action(context) {
            for (const chunk of random(atResponses)) {
                await context.say(chunk);
            }
        }
    },
    {
        pattern: new RegExp(`[^${questionMarks.join('')}](${questionMarks.join('|')})+$`, 'i'),
        cooldown: 15,
        async action(context) {
            for (const chunk of random(eightResponses)) {
                await context.say(chunk);
            }
        }
    },
    {
        pattern: /(hikaru|naka)(\W|$)/i,
        cooldown: 15,
        async action(context) {
            for (const chunk of random(hikaruResponses)) {
                await context.say(chunk.replace('{username}', context.user.username));
            }
        }
    },
    {
        pattern: /checkers|connect (4|four)|tic-?\s*tac-?\s*to/,
        async action(context) {
            await context.say("I’m not being rude, I’m stating a fact, chess is the superior game. It just is. There’s no better game out there. There’s no reason to ever play any game except for chess. If you tell me you like another game, I’ll laugh at you and I’ll shame you.")
            await context.say("How dare you have different taste in games than I, a fan of chess? If you like playing tic-tac-toe, even just for fun, I’ll spit on you. If you have a good time playing connect 4 with your friends on Friday nights, I’ll call you every bad name I can think of.");
            await context.say("There’s no excuse. You need to get off your lazy ass and play chess or I’ll never talk to you again. For those of you who choose to play checkers, you’re not a REAL fan of chess. The only reason the 8x8 board, the board that originally ran chess, the best game in the entire world and the only one anyone should play, sold any units at all was so that people could play chess, the ultimate game.");
        }
    },
//    {
//        trigger: "pepper",
//        async action(context) {
//            await context.say("Pepperoni, the Man: protects his cat like a sane man would protect his own body; will stay with his cat at all costs, preferably riding in the cat-saddle; has a wounded left eye and damaged vision from his laser-pointer misfiring when he was boy; even-tempered and slow-talking; doesn't believe in ghosts.");
//            await context.say("Jesse Jones, the Cat: will try to stay with his rider, even against his command at times; has a fiery temper; loves yarn; and distrusts cat-hunters.");
//        }
//    },
    {
        pattern: /(\W|^)chess(\W|$)/i,
        cooldown: 60,
        async action(context) {
            context.say("Did someone say CHESS?");
        }
    },
    {
        pattern: /overwatch/,
        cooldown: 60,
        async action(context) {
            context.say(`Overwatch requires no skill. It requires zero positioning and no aiming. All you do is go on the point and you win. You know what requires real skill? Chess. Can you accurately defend obscure gambits in the french defense with the black pieces? Didn't think so. Chess requires positioning, tactical expertise, and brilliant game-sense.`);
            context.say(`I am an enlightened warlord, with all the wisdom of Sun Tzu, and the strength of Genghis Khan as my delicate, feminine fingers kiss the bodies of my men, imbuing them with purpose. When I attempt to carry my team in Overwatch as one of the few perfect heroes (who aren’t flawed like the rest of the heroes) such as Zenyatta and Winston, I simply cannot overpower the advanced autism that I am always stuck with, even in my lofty rank of low gold. I would be GM EASILY if it weren’t for my awful teammates.`);
            context.say(`I have decided to uninstall the shitshow of a game Blizzard calls “Overwatch”, but should be known as “Shit”. Anyways, I will be now pursuing my true calling in chess. By the way, none of you sweaty virgins will ever slay as much pussy as me, the General. Step one to getting pussy like me: get some vodka, put roofies in the vodka, and slay bitches. Anyways, peace out Overwatch, you’re a trash game.`);
        }
    }
]
